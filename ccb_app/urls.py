from django.urls import path
from . import views

# app_name = 'ccb_app'

urlpatterns = [
    path('', views.home_page_view, name='home'),
    path('gallery/', views.gallery_page_view, name='gallery'),
    path('accommodation/', views.accommodation_page_view, name='accommodation'),

    path('price/', views.price_page_view, name='price'),
    path('review/', views.review_page_view, name='review'),
    path('language/', views.language_page_view, name='language'),
    path('register/', views.register_page_view, name='register'),
    path('login/', views.login_page_view, name='login'),
    path('admin_page/', views.admin_page_view, name='admin_page'),

    path('room_create/', views.RoomCreateView.as_view(), name='room_create'),
    path('room_all_view/', views.room_all_view, name='room_all_view'),
    path('room_update/<int:pk>/', views.RoomUpdateView.as_view(), name='room_update'),
    path('room_delete/<int:pk>/', views.RoomDeleteView.as_view(), name='room_delete'),
    path('room_details/<int:room_id>/', views.room_details_view, name='room_details'),
    # path('room_details/<slug:slug>/', views.room_details_view, name='room_details'),

    path('image_create/', views.ImageCreateView.as_view(), name='image_create'),
    path('image_all_view/', views.image_all_view, name='image_all_view'),
    path('image_delete/<int:pk>/', views.ImageDeleteView.as_view(), name='image_delete'),

    path('facility/', views.facility_page_view, name='facility'),
    path('facility_create/', views.FacilityCreateView.as_view(), name='facility_create'),
    # path('facility_create/', views.facility_create_view, name='facility_create'),
    path('facility_all_view/', views.facility_all_view, name='facility_all_view'),
    path('facility_update/<int:pk>/', views.FacilityUpdateView.as_view(), name='facility_update'),
    path('facility_delete/<int:pk>/', views.FacilityDeleteView.as_view(), name='facility_delete'),
    path('facility_details/<int:facility_id>/', views.facility_details_view, name='facility_details'),

    path('facility_image_create/', views.FacilityImageCreateView.as_view(), name='facility_image_create'),
    # path('facility_image_all_view/', views.image_all_view, name='image_all_view'),
    path('facility_image_delete/<int:pk>/', views.FacilityImageDeleteView.as_view(), name='facility_image_delete'),


    path('reservation_create/', views.ReservationCreateView.as_view(), name='reservation_create'),
    path('availability/', views.availability_form, name='availability'),
    # path('availability/', views.availability_and_reservation_form, name='availability'),
    path('availabile_rooms/', views.available_rooms_view, name='available_rooms'),
    # path('reservation_all_view/', views.reservations_all_view, name='reservation_all_view'),
    # path('facility_update/<int:pk>/', views.FacilityUpdateView.as_view(), name='facility_update'),
    # path('facility_delete/<int:pk>/', views.FacilityDeleteView.as_view(), name='facility_delete'),
    # path('facility_details/<int:facility_id>/', views.facility_details_view, name='facility_details'),


    # path('room_details/', views.room_details_view, name='room_details'),
    #     fiind home page cea de sus e cu '', restul vor avea field.
    #     atentie la nume, vor trebui sa fie individuale. vezi shop
]
