from django import forms
from django.forms import widgets

from ccb_app.models import *


class ClientProfileForm(forms.ModelForm):
    class Meta:
        model = ClientProfile
        fields = '__all__'
        exclude = ['content']


class RoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = '__all__'


class ImageForm(forms.ModelForm):
    class Meta:
        model = RoomImage
        fields = '__all__'


class FacilityForm(forms.ModelForm):
    class Meta:
        model = Facility
        fields = '__all__'


class FacilityImageForm(forms.ModelForm):
    class Meta:
        model = FacilityImage
        fields = '__all__'


class CreateReservationForm(forms.ModelForm):
    class Meta:
        model = Reservation
        fields = '__all__'
        exclude = ['status']


# class AvailabilityForm(forms.ModelForm):
#     class Meta:
#         model = Reservation
#         fields = '__all__'

class AvailabilityForm(forms.Form):  # asta devine si create reservation -
    # trebuie sa il finalizez cu o rezervare sau sa o rada
    check_in = forms.DateField(label="Data sosire", widget=widgets.DateInput(attrs={'type': 'date'}))
    check_out = forms.DateField(label="Data plecare", widget=widgets.DateInput(attrs={'type': 'date'}))
#     # number_of_guest = 2 be completed later

# def is_valid(self):
#     return self.check_in < self.check_out

# def is_valid(self):  # - nu e acelasi lucru cu cel de mai sus?
#     if self.check_in < self.check_out:
#         return True

# rooms_available = forms.ModelChoiceField(queryset=Room.objects.all())

# def clean(self):
#     super(AvailabilityForm, self).clean()
#     check_in = self.cleaned_data.get('check_in')
#     check_out = self.cleaned_data.get('check_out')
#
#     return self.cleaned_data
