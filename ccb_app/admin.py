from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Room)
admin.site.register(SpecialOffer)
admin.site.register(Reservation)
admin.site.register(ReservationItem)
admin.site.register(ClientProfile)
admin.site.register(Review)
admin.site.register(Comment)
admin.site.register(RoomImage)
admin.site.register(Facility)
admin.site.register(FacilityImage)
