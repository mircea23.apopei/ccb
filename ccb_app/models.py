from datetime import datetime

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models

# Create your models here.
from django.urls import reverse
from django.utils import timezone


# class RoomType(models.Model):
#     name = models.CharField(max_length=50)
#
#     def __str__(self):
#         return f'{self.name}'


class Room(models.Model):
    ROOM_TYPES = (('camera_dubla', 'Camera Dubla'), ('apartament', 'Apartament'), ('facility', 'Facilitate'),
                  ('exterior', 'Exterior'), ('interior', 'Interior'),)
    name = models.CharField(max_length=50)
    # room_type = models.ForeignKey(RoomType, on_delete=models.CASCADE)
    room_type = models.CharField(max_length=30, choices=ROOM_TYPES)
    beds = models.IntegerField()
    capacity = models.IntegerField()
    description = models.TextField()
    # availability = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=19, decimal_places=0)
    slug = models.SlugField()

    # image = models.Ma
    # image = models.ImageField(upload_to='static/room_images/', default='static/not_available.png')

    def __str__(self):
        return f'{self.name}'

    def get_absolute_url(self):
        return reverse("ccb_app:room_details", kwargs={
            'slug': self.slug
        })


class Reservation(models.Model):
    # DE CE ESTE RESERVATION_STATUS SCRIS CU LITERE MARI??? e constanta
    RESERVATION_STATUS = (
        ('searching', 'Cauta'), ('confirmed', 'Confirmata'), ('6pm_release', 'Retinuta'), ('pending', 'In curs de confirmare'),
        ('canceled', 'Anulata'), ('wait_list', 'In asteptare'), ('no_show', 'No show'), ('checked-in', 'Checked-in'),
        ('checked-out', 'Checked-out'))
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # room = models.ForeignKey(Room, on_delete=models.CASCADE)
    check_in = models.DateField(default=datetime.now())
    check_out = models.DateField(default=datetime.now())
    status = models.CharField(max_length=15, choices=RESERVATION_STATUS)
    booked = models.BooleanField(default=False)

    def __str__(self):
        return f'Rezervarea lui {self.user} din {self.check_in} pana in {self.check_out} este {self.status}'


class ReservationItem(models.Model):
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    reservation = models.ForeignKey(Reservation, on_delete=models.CASCADE)
    booked = models.BooleanField(default=False)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return f'in rezervarea lui {self.reservation.user} au intrat {self.room} si s-au dus in {self.reservation}'


class Facility(models.Model):
    FACILITY_TYPES = (('foisor', 'Foisor'), ('ciubar', 'Ciubar'), ('spalatorie', 'Spalatorie'))
    name = models.CharField(max_length=50)
    facility_type = models.CharField(max_length=50, choices=FACILITY_TYPES)
    description = models.TextField()

    def __str__(self):
        return f'{self.name}'


class RoomImage(models.Model):
    image = models.ImageField(null=True, blank=True, upload_to='static/images')
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.image}'


class FacilityImage(models.Model):
    image = models.ImageField(null=True, blank=True, upload_to='static/images')
    facility = models.ForeignKey(Facility, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.image}'


class SpecialOffer(models.Model):
    name = models.CharField(max_length=100)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'{self.name}'


def telephone_valid(value):
    valid_characters = '0123456789-().'
    for character in value:
        if character not in valid_characters:
            raise ValidationError(f"{character} is not a valid character for phone number (accepted only '-().')")
    if value[:2] != "07":
        raise ValidationError(f"{value} is not a Romanian phone number")
    numeric_phone = list(
        filter(lambda character: character not in '-().', value))  # scoate caracterele care nu sunt cifre din value
    print(f"{numeric_phone}")
    if len(numeric_phone) != 10:
        raise ValidationError(f"{value} length is invalid")

    # api de sms


# def email_valid() o functie de validare a adresei de email -
# trimitere email cu link de validare; sau logare cu fbook, gmail

class ClientProfile(models.Model):
    name = models.CharField(max_length=50, default="", name='Nume')
    surname = models.CharField(max_length=50, default="", name='Prenume')
    phone_number = models.CharField(max_length=18, default="0", validators=[
        telephone_valid], name='Numar de telefon')
    email = models.EmailField(null=True, blank=True)  # de adaugat constraint pe view
    content = models.TextField()

    def __str__(self):
        return self.name


class Review(models.Model):
    title = models.CharField(max_length=75)
    content = models.TextField()
    datetime = models.DateTimeField(default=timezone.now)
    reservation_item = models.ForeignKey(Reservation, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Comment(models.Model):
    review = models.ForeignKey(Review, related_name='review', on_delete=models.CASCADE)
    parent_comment = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    author_name = models.ForeignKey(ClientProfile, related_name='author_name', on_delete=models.CASCADE)
    content = models.TextField()
    datetime = models.DateTimeField(auto_now=True)
    accepted = models.BooleanField(default=False)

    class Meta:
        ordering = ['-datetime']

    def __str__(self):
        return f"{self.author_name} - {self.content}"
