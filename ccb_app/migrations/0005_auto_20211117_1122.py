# Generated by Django 3.2.8 on 2021-11-17 09:22

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ccb_app', '0004_auto_20211117_1119'),
    ]

    operations = [
        migrations.AddField(
            model_name='reservationitem',
            name='quantity',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='reservation',
            name='check_in',
            field=models.DateField(default=datetime.datetime(2021, 11, 17, 11, 22, 39, 290317)),
        ),
        migrations.AlterField(
            model_name='reservation',
            name='check_out',
            field=models.DateField(default=datetime.datetime(2021, 11, 17, 11, 22, 39, 290317)),
        ),
    ]
