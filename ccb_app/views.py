from django.contrib.auth.decorators import permission_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import UserPassesTestMixin, PermissionRequiredMixin
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.views import View
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView

from ccb_app.forms import *


# Create your views here.
def home_page_view(request):
    # return HttpResponse('Bun venit la CCB')
    return render(request, 'pages/homepage.html', {})


class StaffRequiredMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_staff


def admin_page_view(request):
    return render(request, 'pages/admin_page.html', {})


class RoomCreateView(PermissionRequiredMixin, StaffRequiredMixin, CreateView):
    permission_required = 'rooms.add_room'
    model = Room
    form_class = RoomForm
    template_name = 'pages/room_create.html'
    success_url = '/room_all_view'


@permission_required("rooms.details_room")  # acest permission inseamna ca acest view nu se face decat daca esti logat
def room_details_view(request, room_id):
    room = Room.objects.get(id=room_id)
    images = RoomImage.objects.filter(room=room)
    return render(request, 'pages/room_details.html',
                  {'room': room,
                   'images': images, })


# def room_details_view(request, slug):
#     room = Room.objects.get(slug=slug)
#     images = RoomImage.objects.filter(room=room)
#     return render(request, 'pages/room_details.html',
#                   {'room': room,
#                    'images': images, })


class RoomUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'rooms.change_room'
    model = Room
    form_class = RoomForm
    template_name = 'pages/room_update.html'
    success_url = '/room_all_view'


class RoomDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
    permission_required = 'rooms.delete_room'
    model = Room
    template_name = 'pages/room_delete.html'
    success_url = '/room_all_view'


# # @permission_required('reservation.create_reservation')
# def reservation_create_page_view(request):
#     if request.method == 'POST':
#         reservation_form = CreateReservationForm(request.POST or None)
#         if reservation_form.is_valid():
#             reservation = reservation_form.save()
#             reservation.save()
#         else:
#             reservation_form = CreateReservationForm()
#         return render(request, 'pages/reservation_create.html', {'reservation_form': reservation_form})


def check_availability(room, check_in, check_out):
    reservation_item_list = ReservationItem.objects.filter(room=room)
    reservation_list = [reservation_item.reservation for reservation_item in reservation_item_list]
    for reservation in reservation_list:
        if reservation.check_in >= check_out or reservation.check_out <= check_in:
            # availability_list.append(True)
            pass
        else:
            # availability_list.append(False)
            return False
    return True


# @StaffStatusDecorator
# def angajat_create_and_register_view(request):
#     if request.method == 'POST':
#         angajat_form = AngajatForm(data=request.POST)
#         user_form = UserCreationForm(data=request.POST)
#         if angajat_form.is_valid() and user_form.is_valid():
#             angajat: Employee = angajat_form.save()
#             user = user_form.save()
#             # salvam profilul de angajat si facem un update la coloana user si legam de user-ul creat mai sus
#             angajat.user = user
#             angajat.save()
#             if '_create' in request.POST:
#                 return redirect('/')
#             elif '_continue' in request.POST:
#                 return redirect(f"/create_contract_details/{angajat.id}/")
#
#     else:  # method=get
#         # instantiem 2 formulare goale si le trimitem catre template
#         angajat_form = AngajatForm()
#         user_form = UserCreationForm()
#
#     # Pe get, formularele sunt goale. Pe post, formularele sunt cele instantiate mai sus in if(populate).
#     return render(request, 'pages/create_and_register_angajat.html',
#                   {'angajat_form': angajat_form, 'user_form': user_form})


# def availability_and_reservation_form(request):
#     submit_button = request.POST.get("submit")
#     check_in = datetime.date(timezone.now())
#     check_out = datetime.date(timezone.now())
#     room_list = Room.objects.all()
#     available_rooms = []
#     reservation_list = Reservation.objects.all()
#     form = AvailabilityForm(request.POST or None)
#     if request.method == 'POST':
#         # submit_button = request.POST.get("submit")
#         # check_in = datetime.date(timezone.now())
#         # check_out = datetime.date(timezone.now())
#         # room_list = Room.objects.all()
#         # available_rooms = []
#         # reservation_list = Reservation.objects.all()
#         # form = AvailabilityForm(request.POST or None)
#         if form.is_valid():
#             check_in = form.cleaned_data.get('check_in')
#             check_out = form.cleaned_data.get('check_out')
#             for room in room_list:
#                 if check_availability(room, check_in, check_out):
#                     available_rooms.append(room)
#                     # for available_room in available_rooms:
#
#         reservation = Reservation.objects.create(check_in=check_in, check_out=check_out, user=request.user,
#                                                  status='searching')
#
#         return render(request, 'pages/availability_view.html', {
#             'reservation': reservation
#         })
#         # 'form': form, 'check_in': check_in,
#         # 'check_out': check_out, 'submit_button': submit_button,
#         # 'available_rooms': available_rooms,
#         # 'room_list': room_list,
#         # 'reservation_list': reservation_list,
#     return render(request, 'pages/availability_view.html', {'form': form, 'check_in': check_in,
#                                                             'check_out': check_out, 'submit_button': submit_button,
#                                                             'available_rooms': available_rooms,
#                                                             'room_list': room_list,
#                                                             'reservation_list': reservation_list,
#
#                                                             })
#     #   'reservation': reservation


def availability_form(request):
    submit_button = request.POST.get("submit")
    check_in = datetime.date(timezone.now())
    check_out = datetime.date(timezone.now())
    room_list = Room.objects.all()
    available_rooms = []
    reservation_list = Reservation.objects.all()

    form = AvailabilityForm(request.POST or None)

    if form.is_valid():
        check_in = form.cleaned_data.get('check_in')
        check_out = form.cleaned_data.get('check_out')
        # reservation = Reservation.objects.create(check_in=check_in, check_out=check_out, user=request.user,
        #                                          status='searching')

    for room in room_list:
        if check_availability(room, check_in, check_out):
            available_rooms.append(room)

    reservation = Reservation.objects.create(check_in=check_in, check_out=check_out, user=request.user,
                                             status='searching')
    for available_room in available_rooms:
        reservation_item = ReservationItem.objects.create(room=available_room)
        reservation_qs = Reservation.objects.filter(user=request.user, booked=False)
        if reservation_qs.exists():
            reservation = reservation_qs[0]
            if reservation.rooms.filter(room__id=available_room.room.room_id).exists():
                reservation_item.quantity += 1
                reservation_item.save()
            else:
                reservation.rooms.add(reservation_item)
        else:
            reservation = Reservation.objects.create(check_in=check_in, check_out=check_out, user=request.user,
                                                     status='searching')
            reservation.rooms.add(reservation_item)
        return HttpResponseRedirect(request.path_info)

    return render(request, 'pages/availability_view.html', {'form': form, 'check_in': check_in,
                                                            'check_out': check_out, 'submit_button': submit_button,
                                                            'available_rooms': available_rooms, 'room_list': room_list,
                                                            'reservation_list': reservation_list,
                                                            'reservation': reservation,
                                                            })  # 'reservation': reservation


def add_to_reservation(request):
    room_id = request.POST.get('room_id')
    reservation_id = request.POST.get('reservation_id')
    room = Room.objects.get(id=room_id)
    reservation_item = ReservationItem.objects.create(
        item=room,
        reservation_id=reservation_id
    )

    return redirect('/')


class ReservationCreateView(CreateView):
    permission_required = 'reservation.create_reservation'
    model = Reservation
    form_class = CreateReservationForm
    template_name = 'pages/reservation_create.html'
    success_url = '/'


# def find_total_room_charge(check_in, check_out, category):
#     days = check_out - check_in
#     room_category = RoomCategory.objects.get(category=category)
#     total = days.days * room_category.rate
#     return total


# if request.method == 'POST':
#     form = AvailabilityForm(request.POST)
#     if form.is_valid():
#         return render(request, 'pages/available_rooms_view.html', {})
#     else:
#         return render(request, 'pages/availability_view.html', {'form': form})
# else:
#     form = AvailabilityForm()
#
# return render(request, 'pages/availability_view.html', {'form': form})


# class AvailabilityFormView(PermissionRequiredMixin, StaffRequiredMixin, CreateView):
#     permission_required = 'availability.check_availability'
#     # model = Reservation
#     form_class = AvailabilityForm
#     template_name = 'pages/availability_view.html'
#     success_url = 'pages/available_rooms_view.html'


def available_rooms_view(request):
    # form = AvailabilityForm
    # room_list = Room.objects.all()
    # available_rooms = []
    #
    # if form.is_valid():
    #     data = form.cleaned_data
    #
    # for room in room_list:
    #     if check_availability(room, data['check-in'], data['check-out']):
    #         available_rooms.append(room)

    return render(request, 'pages/available_rooms_view.html', {})


# class ReservationCreateView(CreateView):
#     permission_required = 'reservation.create_reservation'
#     model = Reservation
#     form_class = CreateReservationForm
#     template_name = 'pages/reservation_create.html'
#     success_url = '/'


# @permission_required('facility.create_facility')
# @StaffStatusDecorator
# def reservations_all_view(request):
#     reservations = ReservationItem.objects.all()
#     return render(request, 'pages/reservation_all_view.html', {'reservations': reservations})


class ImageCreateView(PermissionRequiredMixin, StaffRequiredMixin, CreateView):
    permission_required = 'image.create_image'
    model = RoomImage
    form_class = ImageForm
    template_name = 'pages/image_create.html'
    success_url = '/room_all_view'


class ImageDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
    permission_required = 'image.delete_image'
    model = RoomImage
    template_name = 'pages/image_delete.html'
    success_url = '/image_all_view'


class StaffStatusDecorator:
    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        request = args[0]
        if request.user.is_staff:
            result = self.func(*args, **kwargs)
            return result
        else:
            return HttpResponseForbidden()


# @permission_required('rooms.view_room')
@StaffStatusDecorator
def room_all_view(request):
    all_rooms = Room.objects.all()
    return render(request, 'pages/room_all_view.html', {'rooms': all_rooms})


@permission_required('image.view_image')
@StaffStatusDecorator
def image_all_view(request):
    all_images = RoomImage.objects.all()
    all_facility_images = FacilityImage.objects.all()
    return render(request, 'pages/image_all_view.html', {'images': all_images, 'facility_images': all_facility_images})


@permission_required('facility.view_facility')
@StaffStatusDecorator
def facility_all_view(request):
    all_facilities = Facility.objects.all()
    return render(request, 'pages/facility_all_view.html', {'facilities': all_facilities, })


def gallery_page_view(request):
    all_images = RoomImage.objects.all()
    all_facility_images = FacilityImage.objects.all()

    return render(request, 'pages/gallery.html', {'images': all_images, 'facility_images': all_facility_images})


def accommodation_page_view(request):
    all_rooms = Room.objects.all()
    all_double_rooms = Room.objects.filter(room_type='camera_dubla')
    all_apartments = Room.objects.filter(room_type='apartament')
    rooms_with_images = []
    apartments_with_images = []
    for room in all_double_rooms:
        images = RoomImage.objects.filter(room=room)
        rooms_with_images.append({'room': room, 'images': images})
    for apartment in all_apartments:
        images = RoomImage.objects.filter(room=apartment)
        apartments_with_images.append({'room': apartment, 'images': images})
    return render(request, 'pages/accommodation.html', {'all_rooms': all_rooms,
                                                        'all_double_rooms': all_double_rooms,
                                                        'all_apartments': all_apartments,
                                                        'rooms_with_images': rooms_with_images,
                                                        'apartments_with_images': apartments_with_images,
                                                        })


# @permission_required('facility.create_facility')
# @StaffStatusDecorator
# def facility_create_view(request):
#     if request.method == 'POST':
#     facility_form = FacilityForm(data=request.POST)
#     if facility_form.is_valid():
#         facility = facility_form.save()
#         facility.save()
#     else:
#          facility_form = FacilityForm()
#     return render(request, 'pages/facility_create.html', {'facility_form': facility_form})


class FacilityCreateView(PermissionRequiredMixin, StaffRequiredMixin, CreateView):
    permission_required = 'facility.create_facility'
    model = Facility
    form_class = FacilityForm
    template_name = 'pages/facility_create.html'
    success_url = '/facility_all_view'


class FacilityImageCreateView(PermissionRequiredMixin, StaffRequiredMixin, CreateView):
    permission_required = 'facility_image.create_image'
    model = FacilityImage
    form_class = FacilityImageForm
    template_name = 'pages/facility_image_create.html'
    success_url = '/facility_all_view'


class FacilityImageDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
    permission_required = 'facility_image.delete_facility'
    model = FacilityImage
    template_name = 'pages/facility_image_delete.html'
    success_url = '/image_all_view'


# @permission_required("facility.details_facility")
def facility_details_view(request, facility_id):
    facility = Facility.objects.get(id=facility_id)
    images = FacilityImage.objects.filter(facility=facility)
    return render(request, 'pages/facility_details.html',
                  {'facility': facility,
                   'images': images, })


class FacilityUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'facility.change_facility'
    model = Facility
    form_class = FacilityForm
    template_name = 'pages/facility_update.html'
    success_url = '/facility_all_view'


class FacilityDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
    permission_required = 'facility.delete_facility'
    model = Facility
    template_name = 'pages/facility_delete.html'
    success_url = '/facility_all_view'


def facility_page_view(request):
    all_facilities = Facility.objects.all()
    facilities_with_images = []
    for facility in all_facilities:
        images = FacilityImage.objects.filter(facility=facility)
        facilities_with_images.append({'facility': facility, 'images': images})
    return render(request, 'pages/facility.html',
                  {'all_facilities': all_facilities, 'facilities_with_images': facilities_with_images, })


def price_page_view(request):
    return render(request, 'pages/price.html', {})


def review_page_view(request):
    return render(request, 'pages/review.html', {})


def language_page_view(request):
    return render(request, 'pages/language.html', {})


def register_page_view(request):
    if request.method == 'POST':
        user_form = UserCreationForm(data=request.POST)
        client_profile_form = ClientProfileForm(data=request.POST)
        if user_form.is_valid() and client_profile_form.is_valid():
            user = user_form.save()
            client_profile = client_profile_form.save()
            client_profile.user = user
            client_profile.save()
            return redirect('/')
    else:
        user_form = UserCreationForm()
        client_profile_form = ClientProfileForm()
    return render(request, 'pages/register.html', {'user_form': user_form, 'client_profile_form': client_profile_form})


def login_page_view(request):
    return render(request, 'registration/login.html', {})
