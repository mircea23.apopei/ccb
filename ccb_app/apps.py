from django.apps import AppConfig


class CcbAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ccb_app'
